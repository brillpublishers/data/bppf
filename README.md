# Brill - Past, Present, Future

This is a repo for publication _about_ Brill.

Currently, it is only _Brill325_ (both NL and EN versions). The idea is to add content, e.g. materials from the launch of the Brill font, the exhibition _Geletterd en geleerd_, etc.
